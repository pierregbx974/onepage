
# LES COMMUNS 
Pérénniser et gérer ensemble nos ressources

Des ressources, gérées collectivement par une communauté, qui établit des règles et une gouvernance
dans le but de préserver et pérenniser cette ressource

La définition des communs est un chantier à part entière toujours en cours, à l'image de leur diversité.

## RESSOURCES

## NATURELLES
- Eau, mer, rivières,...
- Terres, Potagers (urbains, partagés), semences, forêts,...
- Poissons, ressources,...
- Énergie libre (solaire, géothermie, éolien)

## MATERIELLES
- Internet
- Centre de santé 
- Machines outils (FabLab, Bibliothèque d’outils )
- AMAP , épicerie Coopérative, Disco Soupe
- Tiers Lieux Open Source

## IMMATÉRielles (INTELLECTUELLES)
- La culture
- Musique : les grands classiques 
- Livres, BD
- Art libre
- Les savoirs-faire
- Un Logiciel Libre (Open Source)
- Une recette de cuisine
- Un repair café
- Les connaissances et les données
- Sciences Ouvertes (un code génétique, des résultats de recherche,...)
- Wikipédia
- Open Street Maps
- OpenFoodFact

## Communauté
- Groupe de personnes solidaires, liées par une responsabilité commune, des intérêts communs
- Petite ou grande, en fonction du commun
- Locale, internationale en fonction des besoins

## Gouvernance
- La communauté choisit son mode de gouvernance
- Holacratie, doocratie, sociocratie, démocratie directe... selon les besoins du commun
- Les règles d'utilisation, de gestion, de protection, de contribution et d'accès au commun sont définies par la communauté 
- Avoir un “Code Social” Ouvert et Transparent

## Economie
- Des contributions rétribuées
- Des échanges basés sur la réciprocité

## Participer
- Créer des assemblées d'acteurs des communs, 
- Mettre en lumière leur existence
- Préserver des "enclosures" et autres modes d'accaparement
- Faire société, participer à la gestion de sa commune

- Festivals artistiques 
- Education
- Hackerspace/Makerspace Finances et monnaies solidaires
- Mise en commun de propriété 
- Achat groupé
- Gestion des terres 
- Solidarité internationale
- Grainothèques 
- Réseau de voisinage
- Tiers-Lieux / Centre de ressources / CULTURE ARTS 
- Récupération et cuisine de légumes de manière festive
- Restaurant Cuisine Café 
- Covoiturage
- Jardins partagés 
- Réseau de logements entre habitants, habitat partagé,
- autopromotion immobilière
- Réappropriation de délaissés urbains par les citoyens
- (incroyables comestibles,...)
- Plateforme de projets
- Magasin/ Epicerie 
- Santé
- Bricolage : outilthèques, matériauthèques 
- Finances et monnaies solidaires
- Logiciel libre 
- Mobilité
- Données ouvertes 
- Informatique libre et collaborative
- Information auto-produite collectivement 
- Réseau associatif
- Plans de matériel libre 
- Débat
- Encyclopédie 
- Coworking/Espace de travail partagé


# CITOYENNETÉ 
Territiore Hautement Citoyen

intégrer un projet de société agile et adapté pour toutes les générations
Etre citoyen, c’est comprendre le système, le créer, l’adapter et l’améliorer
