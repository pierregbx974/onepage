
# Alimentation Santé
Territoire Producteur Consomm’Acteur

Devenir acteur de sa santé en choisissant une alimentation locale “vivante”
Respectueuse de l’homme et de l'environnement.

### Alimentation
- Modifier notre mode alimentaire (rechercher “l’équilibre acido-basique”)
- Offrir plus de place aux aliments non transformés par la cuisson et l’industrie (plus je me rapproche de ce que m’offre la - - terre, plus les aliments sont simples)
- moins je me pollue, moins je pollue
- Expérimenter la richesse de la nature et “Sa” propre créativité pour soutenir et accompagner nos traitements 

### Bien être
- Retour aux sources, aux racines
- Nourrir son RIRE
- Expérimentons les effets du Partage et des actions solidaires
- Comprendre l’énergie de la nature, de notre nature : Chi Kong, Thai Chi, Yoga, Méditation, être soi dans la pleine conscience

### Education 
- Découvrons l’alimentation “vivante” comme des enfants
- Retombons en enfance pour nos enfants !
- Nous sommes un produit de la nature, renouons le lien avec elle, qui nous donne tout sans rien attendre en retour

### PRODUIRE LOCALEMENT
- AMAP Péi Bio ( Producteurs locaux )
- Mon ti Jardin 

### Santé
- Prendre de soin de sa terre, c’est prendre soin de soi
- La “bonne santé” de nos aliments contribue à la nôtre
- La qualité du  “Bonheur Intérieur Brut” de l’être humain est étroitement liée à sa santé, à son environnement

### CONSOMM’ACTEUR
- Choisir ses aliments en pleine conscience/prendre le temps…
- Économique (privilégier les circuits courts/valoriser les ressources locales)
- Écologique ( choisir des produits “sains”)

### Main de L’homme
- Massages
- Reiki, Kinésiologie,soins energétiques
- Microkinésithérapie
- Réflexologie
- Plantes
- Plantes Médicinales
- Découvrir ses essences : Phytothérapie, Aromathérapie, Tisaneurs, Huiles essentielles

### Attention
- Les produits BIO industriels restent des produits industriels transformés ! 
- aux pressions des médias, génératrices de croyances (prise de recul face à toutes les injonctions publicitaires)
- Vous êtes unique: ce qui est bon pour l’un, ne l’est pas forcement pour vous

### Proverbe
Manger la moitié, marcher le double, 
rire le triple et aimer sans mesure. 
